SMS = new Mongo.Collection('sms');

SMS.allow({
	'insert': function(doc) {
		return doc;
	},
	'update': function(userId, doc, fields, modifier) {
		return userId === doc.userId;
	}
});