Meteor.users.allow({
	insert: function (userId) {
		return (userId ? true : false);
	},
	update: function (userId, user, fields, modifier) {
		if (user._id !== userId)
			return false;

		if (fields.length !== 1 || fields[0] !== 'profile')
			return false;

		return true;
	},
	fetch: ['_id']
});

Meteor.users.deny({  
	update: function() {
		return true;
	}
});

Meteor.methods({
	userDetailsUpdate: function (profile) {
		profile.updated = new Date();

		var updateId = Meteor.users.update(this.userId, { $set: { 'userDetails.profile': profile } });
		return updateId;
	},
	memberDetailsUpdate: function (member) {
		var memberDetail = {
			name: member.name,
			dob: member.dob,
			gender: member.gender
		};

		memberDetail.updated = new Date();

		var updateId = Meteor.users.update(member.userId, { $set: { 'userDetails.profile': memberDetail } });
		return updateId;
	},
	userAddress: function (address) {
		address.added = new Date();

		var updateId = Meteor.users.update(this.userId, { $push: { 'userDetails.address': address } });
		return updateId;
	},
	relation: function (member) {
		check(member, {
			type: String,
			userId: String,
			name: String,
			phone: String
		});

		member.added = new Date();

		var updateId = Meteor.users.update(this.userId, { $push: { 'userDetails.members': member } });
		return updateId;
	}
});