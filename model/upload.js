Uploads = new Mongo.Collection('uploads');

Slingshot.fileRestrictions("myFileUploads", {
	allowedFileTypes: ["image/png", "image/jpeg", "image/gif"],
	maxSize: 10 * 1024 * 1024
});

Uploads.allow({
	'insert': function(userId, doc) {
		return userId;
	},
	'update': function(userId, doc, fields, modifier) {
		return userId === doc.userId;
	}
});