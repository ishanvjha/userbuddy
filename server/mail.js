Meteor.methods({
  sendEmail: function (to, from, subject, text) {
    check([to, from, subject, text], [String]);

    this.unblock();

    Email.send({
      to: to,
      from: from,
      subject: subject,
      text: text
    });
  },
  cbMail: function(actionType, data, date, time) {
    SSR.compileTemplate( 'appointmentEmail', Assets.getText( 'newAppointment.html' ) );
    SSR.compileTemplate( 'newMemberEmail', Assets.getText( 'newMember.html' ) );
    SSR.compileTemplate( 'newUserEmail', Assets.getText( 'newUser.html' ) );
    SSR.compileTemplate( 'newOrder', Assets.getText( 'medicineOrder.html' ) );
    SSR.compileTemplate( 'newMsg', Assets.getText( 'newMsg.html' ) );

    var to = "app.carebuddy@gmail.com";
    var from = actionType + " <care@carebuddy.co>";
    var template;
    var subject;
    var emailData;

    if(actionType == 'User Appointment') {
      template = 'appointmentEmail';
      subject = data.type.name + ' added by - ' + Meteor.user().username;
      emailData = {
        type: data.type.name,
        name: data.name,
        phone: data.phone,
        address: data.address,
        comments: data.comments,
        date: date,
        time: time,
        user: data.userId.profile.name + ' (' + data.userId.profile.phone + ')',
        createdBy : Meteor.user().username,
        createdAt : new Date()
      };
    } else if(actionType == 'New Member') {
      template = 'newMemberEmail';
      subject = actionType + ' added by - ' + Meteor.user().username;
      emailData = {
        name: data.name,
        phone: data.phone,
        gender: data.gender,
        relationType: data.relation,
        relationOther: data.other,
        user: Meteor.user().username,
        createdAt : new Date()
      };
    } else if(actionType == 'New User Registration') {
      template = 'newUserEmail';
      subject = actionType + ' (' + data.username + ')';
      emailData = {
        username: data.username,
        email: data.email,
        createdAt : new Date()
      };
    } else if(actionType == 'New Message') {
      template = 'newMsg';
      subject = actionType + ' from ' + Meteor.user().username;
      emailData = {
        msg: data,
        date: date,
        time: time
      };
    } else if(actionType == 'Medicine Order') {
      template = 'newOrder';
      subject = actionType + ' from ' + Meteor.user().username;
      emailData = {
        file: data.url,
        medicineName: data.medicineName,
        comments: data.comments,
        house: data.house,
        street: data.street,
        location: data.location,
        pin: data.pin,
        user:data.user,
        createdAt : new Date()
      };
    }

    Email.send({
      to: to,
      from: from,
      subject: subject,
      html: SSR.render( template, emailData )
    });
  }
});