if (Meteor.isServer) {
	Meteor.publishComposite('chats', function () {
		if (! this.userId) {
			return;
		}

		return {
			find: function () {
				return Chats.find({ userIds: this.userId });
			},
			children: [
			{
				find: function (chat) {
					return Messages.find({ chatId: chat._id });
				}
			},
			{
				find: function (chat) {
					var fields = { profile: 1 };
					return Meteor.users.find({ _id: chat.userIds }, { fields: fields });
				}
			}
			]
		}
	});

	Meteor.publish("userData", function () {
		if (this.userId) {
			const fields = { userDetails: 1, email: 1, phone: 1, status: 1 };
			return Meteor.users.find({ _id: this.userId }, { fields: fields });
		} else {
			this.ready();
		}
	});

	Meteor.publish('members', function () {
		const fields = { userDetails: 1, email: 1, phone: 1, status: 1 };
		return Meteor.users.find({ }, { fields: fields });
	});

	Meteor.publish('carebuddies', function () {
		return Meteor.users.find({ 'roles': 'carebuddy' }, { fields: { _id: 1 } });
	});

	Meteor.publish('records', function () {
		return Uploads.find();
	});

	Meteor.publish('reminders', function () {
		return Reminders.find();
	});

	Meteor.publish('medicines', function () {
		return Medicines.find();
	});

	Meteor.publish('appointments', function (userId) {
		return Appointments.find({ user: userId });
	});
}