Template.logout.onRendered(function() {
	Meteor.logout(function(error){
		if ( error ){
			Bert.alert( error.reason, 'danger', 'growl-top-right' );
		} else {
			Object.keys(Session.keys).forEach(function(key){
				Session.set(key, undefined);
			});
			Session.keys = {}
			FlowRouter.go('login');
		}
	});
});