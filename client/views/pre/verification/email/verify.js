Template.verify.onRendered(function() {
	$('#otp-form').validate({
		rules: {	
			otp: {
				required: true
			}
		}, 
		messages: {
			otp: {
				required: "Please enter one time password."
			}
		},
		submitHandler() {
			const username = $('[name=username]').val(),
			password = $('[name=password]').val(),
			otp = $('[name=otp]').val(),
			email = $('[name=email]').val();

			Meteor.call('verify', username, otp, function(verError, verResponse){
				if (verError) {
					document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: verError.reason });
				} else {
					if(verResponse === true) {
						var credentials = {
							username: username,
							email: email
						};
						Meteor.call('newMember', {
							username  : username,
							email     : email,
							password  : password
						}, function(nmError, nmResponsee){
							if (nmError) {
								document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: nmError.reason });
							} else {
								document.querySelector('dialog').close();
								document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'Login to continue.' });
								Meteor.call( 'cbMail', 'New User Registration', credentials);
								FlowRouter.go('/login');
							}
						});				
					} else {
						document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'OTP not correct' });
					}
				}
			});
		}
	});
});

Template.verify.events({
	'submit form': function( event, template ) {
		event.preventDefault();
	},
	'click .close': function() {
		document.querySelector('dialog').close();
	}
});