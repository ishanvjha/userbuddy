Template.login.onRendered(function() {
	$('#login-form').validate({
		rules: {	
			username: {
				required: true
			},
			password: {
				required: true
			}
		}, 
		messages: {
			username: {
				required: "Username is required"
			}, 
			password: {
				required: "Password is required"
			}
		},
		submitHandler() {
			const userPhone =  $('[name="username"]').val(),
			password = $('[name="password"]').val();

			Meteor.call('verifyDetails', userPhone, function(vdError, vdResponse){
				if (vdError) {
					Bert.alert( vdError.reason, 'danger', 'growl-top-right' );
				} else {
					if(_.isEqual(vdResponse, true)) {
						Meteor.loginWithPassword(userPhone, password, function(error) {
							if (error) {
								Bert.alert( error.reason, 'danger', 'growl-top-right' );
							} else {
								FlowRouter.go('/dashboard/home');
							}
						});
					}
				}
			});
		}
	});
});

Template.login.events({
	'submit form': function( event, template ) {
		event.preventDefault();
	},
	'click #fbLogin': function( event, template ) {
		Meteor.loginWithFacebook({
			requestPermissions: ['public_profile', 'email', 'user_friends']
		}, function (err, response) {
			if (err) {
				Bert.alert( err.reason, 'danger', 'growl-top-right' );
			} else {
				document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'Logged In' });
				FlowRouter.go('/dashboard/home');
			}
		});
	},
	'click #show-dialog': function() {
		var showDialogButton = document.querySelector('dialog#show-dialog');
		if (! showDialogButton.show) {
			dialogPolyfill.registerDialog(showDialogButton);
		}
		showDialogButton.show();
	}
});