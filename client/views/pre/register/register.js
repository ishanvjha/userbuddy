Template.register.onRendered(function() {
	$('#register-form').validate({
		rules: {	
			username: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			newPassword: {
				required: true
			},
			confirmNewPassword: {
				required: true
			}
		}, 
		messages: {
			username: {
				required: "Username is required.",
			},
			email: {
				required: "Email address is required.",
				email: "Email address is not valid."
			},
			newPassword: {
				required: "Password is required."
			},
			confirmNewPassword: {
				required: "Confirm Password is required."
			}
		},
		submitHandler() {
			const userPhone = $('[name=username]').val(),
			email = $('[name=email]').val(),
			newPassword = $('[name=newPassword]').val(),
			confirmNewPassword = $('[name=confirmNewPassword]').val();

			if (_.isEqual(newPassword, confirmNewPassword)) {
				const options = {
					phone: userPhone,
					email: email,
					password: newPassword
				};

				Meteor.call('newMember', {
					username  : userPhone,
					email: email,
					password  : newPassword
				}, function(nmError, nmResponsee){
					if (nmError) {
						Bert.alert( nmError.reason, 'danger', 'growl-top-right' );
					} else {
						Meteor.call('otp', userPhone, function(error, response){
							if (error) {
								Bert.alert( error.reason, 'danger', 'growl-top-right' );
							} else {
								Bert.alert( 'OTP sent to ' + userPhone, 'info', 'growl-top-right' );
								
								Session.set('userPhone', userPhone);
								Session.set('userEmail', email);
								
								var queryParams = { userPhone: userPhone, email: email };
								FlowRouter.go(FlowRouter.path("phoneNumberVerification", '', queryParams));
								Meteor.call( 'cbMail', 'New User Registration', options	);
							}
						});
					}
				});	
			} else {
				Bert.alert( 'Passwords not matching', 'danger', 'growl-top-right' );
			}
		}
	});
});

Template.register.events({
	'submit form': function( event ) {
		event.preventDefault();
	}
});