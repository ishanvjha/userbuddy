Template.password.onRendered(function() {
	$('#password-form').validate({
		rules: {	
			newPassword: {
				required: true
			},
			confirmNewPassword: {
				required: true
			}
		}, 
		messages: {
			newPassword: {
				required: "Please enter new password"
			},
			confirmNewPassword: {
				required: "Please confirm your password"
			}
		},
		submitHandler() {
			const newPassword = $('[name=newPassword]').val(),
			confirmNewPassword = $('[name=confirmNewPassword]').val();

			urlUserPhone = FlowRouter.getQueryParam('userPhone');
			sessionUserPhone = Session.get('userPhone');

			if (_.isEqual(urlUserPhone, sessionUserPhone)) {
				userPhone = sessionUserPhone;

				if(newPassword != confirmNewPassword) {
					Bert.alert( 'Passwords are not matching', 'danger', 'growl-top-right' );
				} else if(newPassword === confirmNewPassword) {
					
					var credentials = {
						username: userPhone
					};

					Meteor.call('newMember', {
						phone  : userPhone,
						password  : newPassword
					}, function(error, responsee){
						if (error) {
							Bert.alert( error.reason, 'danger', 'growl-top-right' );
						} else {
							document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'Login to continue.' });
							Meteor.call( 'cbMail', 'New User Registration', credentials);
							FlowRouter.go('/login');
						}
					});
				}
			} else {
				Bert.alert( 'incorrect Phone Number', 'danger', 'growl-top-right' );
			}
		}
	});
});

Template.password.events({
	'submit form': function( event ) {
		event.preventDefault();
	}
});