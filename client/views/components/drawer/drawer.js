Template.sidenav.events({
	'click .mdl-navigation__link': function( event ) {
		//event.preventDefault();
		// Tracker.autorun(function() {
		// 	FlowRouter.watchPathChange();
		// 	var context = FlowRouter.current();
		// 	console.log(context);
			document.querySelector('.mdl-layout').MaterialLayout.toggleDrawer();
		//});
		//$('.mdl-layout__drawer').toggleClass('is-visible');
	}
});

Template.sidenav.helpers({
	user: function () {
		if (Meteor.user()) {
			if( Meteor.user().userDetails && Meteor.user().userDetails.profile) {
				return {
					name: Meteor.user().userDetails.profile.name,
					phone: Meteor.user().username
				};
			} else {
				return {
					phone: Meteor.user().username
				};
			}
		}
	}
});