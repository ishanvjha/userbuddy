Template.feedback.events({
	'click .feedback-btn': function() {
		$('.feedback-modal').fadeIn();
	},
	'click .mdi-close': function() {
		$('.feedback-modal').fadeOut();
	},
	'submit form': function(event) {
		event.preventDefault();
		var feedback = $('[name=feedback]').val();

		var email = Meteor.user().emails[0].address;
		var from;

		if(_.isEmpty(email)) {
			from = Meteor.user().username;
		} else {
			from = email;
		}

		Meteor.call(
			'sendEmail',
			'app.carebuddy@gmail.com',
			from,
			'Feedback from app',
			feedback,
			function(error, result) {
				if(error) {
					toastr.error(error.reason);
				} else {
					toastr.success('Thanks for your valueable feedback.');
					$('.feedback-modal').fadeOut();
					document.getElementsByClassName('feedback-form').reset();
				}
			});
	}
});