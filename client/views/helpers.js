Handlebars.registerHelper('activePage', function() {
	var routeNames = arguments;
	return _.include(routeNames, Router.current().route.name) && 'active';
});

Template.registerHelper('formatTime', function(context, options) {
	if(context)
		return moment(context).format('dddd, MMMM Do YYYY, h:mm:ss a');
});

Template.registerHelper('user', function() {
	if(Meteor.user()) {
		return Meteor.user();
	} else {
		return;
	}
});

Template.registerHelper('equals', function(v1, v2) {
	if (v1 == v2) {
		return true;
	} else {
		return false;
	}
});

Template.registerHelper('camera', function(file) {
	Session.set('url', '');
	var uploader = new Slingshot.Upload("myFileUploads");
	uploader.send(file, function (error, response) {
		if (error) {
			return error;
		} else {
			Session.set('url', response);
		}
	});
	return uploader;
});

Template.registerHelper('cordova', function() {
	if(Meteor.isCordova) {
		return true;
	} else {
		return false;
	}
});