Template.medicineOrder.onCreated(function() {
	Session.set('uploadResponse', '');
	Session.set('uploadPreview', '');
	Session.set('url', '');
});

Template.medicineOrder.onRendered(function() {
	this.subscribe('members');
	$('#order-form').validate({
		rules: {	
			medicineName: {
				required: true
			},
			house: {
				required: true
			},
			street: {
				required: true
			},
			location: {
				required: true
			},
			pin: {
				required: true
			},
			medUser: {
				required: true
			}
		}, 
		messages: {
			medicineName: {
				required: "Medicine Name is required."
			},
			house: {
				required: "Address field is required."
			},
			street: {
				required: "Address field is required."
			},
			location: {
				required: "Location is required."
			},
			pin: {
				required: "Pin code is required."
			},
			medUser: {
				required: "Select a patient."
			}
		},
		submitHandler() {
			const data = {
				url: Session.get('url'),
				medicineName: $('[name=medicineName').val(),
				comments: $('[name=comments]').val(),
				house: $('[name=house').val(),
				street: $('[name=street]').val(),
				location: $('[name=location]').val(),
				pin: $('[name=pin]').val(),
				user: $('[name=medUser]').val(),
				createdAt : new Date()
			};

			if(_.isEmpty(Session.get('url'))) { 
				Bert.alert({
					title: 'Upload in progress',
					message: 'Please wait while the file upload is completed',
					type: 'info',
					style: 'growl-top-right',
					icon: 'fa-info'
				});
			} else {
				// {
				// 	url: Session.get('url'),
				// 	medicineName: medicineName,
				// 	comments: comments,
				// 	house: house,
				// 	street: street,
				// 	location: location,
				// 	pin: pin,
				// 	user: user,
				// 	createdAt : new Date()
				// }
				Medicines.insert(data, function(eor,ros) {
					if(eor) {
						Bert.alert({
							title: 'Error',
							message: eor.reason,
							type: 'danger',
							style: 'growl-top-right',
							icon: 'fa-exclamation'
						});
					} else {
						Bert.alert({
							title: 'Order successful',
							message: 'Your order has been placed',
							type: 'success',
							style: 'growl-top-right',
							icon: 'fa-check'
						});
						document.getElementById('order-form').reset();
						document.querySelector('dialog#medicineOrder').close();
						Session.clear('url');

						Meteor.call( 'cbMail', 'Medicine Order', data);

						var cbs = Meteor.users.find({ _id: { $ne: Meteor.userId() } }).fetch();

						if(Meteor.user().profile) {
							if(Meteor.user().profile.firstname) {
								userName = Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname;
							} else if(Meteor.user().profile.name) {
								userName = Meteor.user().profile.name;
							}
						} else if(Meteor.user().userDetails && Meteor.user().userDetails.profile) {
							userName = Meteor.user().userDetails.profile.name;
						} else {
							userName = Meteor.user().username;
						}

						_.map(cbs, function(num){
							Meteor.call('browserChatNotification', {
								userId: num._id,
								title: 'New Medicine order',
								body: userName + ' has added a new medicine order',
							}, function(error, result){
								if (error) {
									console.log(error);
								}
							});
						});
					}
				});
			}
		}
	});
});

Template.medicineOrder.helpers({
	user: function() {
		if( Meteor.user() && Meteor.user().userDetails && Meteor.user().userDetails.profile && Meteor.user().userDetails.profile.name ) {
			const userProfile = {
				userId: Meteor.user()._id,
				userName: Meteor.user().userDetails.profile.name
			};
			return userProfile;
		} else {
			const userProfile = {
				userId: Meteor.user()._id,
				userName: Meteor.user().username
			};
			return userProfile;
		}
	},
	allMembers: function () {
		if( Meteor.user() && Meteor.user().userDetails && Meteor.user().userDetails.members ) {
			return Meteor.user().userDetails.members;
		}
	}
	// img: function() {
	// 	$('.pic-select').css('display', 'none');
	// 	$('.image-add-part').css('display', 'block');
	// 	return Session.get('uploadPreview');
	// }
});

Template.medicineOrder.events({
	'click .camera': function( event, template ) {
		var cameraOptions = {
			width: 800,
			height: 800,
			quality: 100
		};
		MeteorCameraUI.getPicture(cameraOptions, function (error, data) {
			if (error) {
				Bert.alert( error.reason, 'danger', 'growl-top-right' );
			} else {
				file = MeteorCameraUI.dataURIToBlob(data);
				file.name = Meteor.userId() + '-' +Date.now() + '.' + file.type.split(/\//).pop();
				NProgress.start();
				if(file) {
					result = Blaze._globalHelpers.camera(file);
					Session.set('uploadPreview', result.url(true));
					Tracker.autorun(function (computation) {
						if (! isNaN(result.progress())){
							NProgress.set(result.progress());
							return Session.set('url', Math.round(result.progress() * 100));
						}
						NProgress.set(result.progress());
						return;
					});
				}
			}
		});
	},
	'change #myFileInput': function( event, template ) {
		var file = document.getElementById('myFileInput').files[0];

		if(file) {
			NProgress.start();

			Session.set('url', '');
			var uploader = new Slingshot.Upload("myFileUploads");

			uploader.send(file, function (upError, response) {

				if (upError) {
					NProgress.remove();
					Bert.alert( uploader.xhr.response, 'danger', 'growl-top-right' );
					Bert.alert( upError.reason, 'danger', 'growl-top-right' );
				} else {

					Tracker.autorun(function (computation) {
						if (! isNaN(uploader.progress())){
							NProgress.set(uploader.progress());
							return Session.set('uploadResponse', Math.round(uploader.progress() * 100));
						}
					});
					
					Bert.alert( 'Upload complete', 'info', 'growl-top-right' );
					Session.set('url', response);
					//Session.set('uploadPreview', uploader.url(true));
				}
			});
		}
	},
	'submit form': function(event) {
		event.preventDefault();
	},
	'click .close-medicineOrder': function () {
		var dialog = document.querySelector('dialog#medicineOrder');
		dialog.close();
	}
});