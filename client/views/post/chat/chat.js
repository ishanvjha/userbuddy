Template.home.onRendered(function() {
	// var $id= $('.chat');
	// $id.scrollTop($id[0].scrollHeight);
	
	var self = this;
	self.autorun(function() {
		self.subscribe('carebuddies');  
	});
	
	Meteor.setTimeout(function() {
		var $id= $('.chat');
		$('.chat').scrollTop($('.chat')[0].scrollHeight);
	}, 500);
});

Template.home.helpers({
	messages: function () {
		var handle = Meteor.subscribe('chats', Meteor.userId());
		Tracker.autorun(function() {
			if (handle.ready()) { 
				var chat = Chats.findOne({});
				if(_.isEmpty(chat)) {
					Meteor.call('newChat', Meteor.userId(), function(ncError, result) {
						if (ncError) {
							document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: ncError.reason });
						} else {
							Session.set('chatId', result);
							Meteor.call('welcomeMessage', {
								text: "Hello there , thanks for using our app. If you need any help, just let me know. I'm available to answer any questions you may have.",
								type: 'text',
								chatId: result
							});
						}
					});
				}
				if(chat) {
					Session.set('chatId', chat._id);
				}
			}
		}); 
		return Messages.find().fetch();
	},

	equals: function(v1) {
		if(v1 == Meteor.userId()){
			return true;
		} else {
			return false;
		}
	},

	textType: function(v1, v2) {
		if (v1 == v2) {
			return true;
		} else {
			return false;
		}
	}
});

Template.home.events({
	'click #submit, keypress': function( event, template ) {
		if ((event.type === 'click') || (event.type === 'keypress' && event.which === 13) ) {
			event.preventDefault();
			var msg = $('input[name=message]').val();
			if (_.isEmpty(msg)) {
				return;
			}
			$('[name=message]').val('');
			Meteor.call('newMessage', {
				text: msg,
				type: 'text',
				chatId: Session.get('chatId')
			}, function(error, result){
				if (error) {
					Materialize.toast('Failed', 2000, 'rounded');
				} else {
					var cbs = Meteor.users.find({ _id: { $ne: Meteor.userId() } }).fetch();

					if(Meteor.user().profile) {
						if(Meteor.user().profile.firstname) {
							userName = Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname;
						} else if(Meteor.user().profile.name) {
							userName = Meteor.user().profile.name;
						}
					} else if(Meteor.user().userDetails && Meteor.user().userDetails.profile) {
						userName = Meteor.user().userDetails.profile.name;
					} else {
						userName = Meteor.user().username;
					}

					_.map(cbs, function(num){
						Meteor.call('browserChatNotification', {
							userId: num._id,
							title: 'New Message from ' + userName,
							body: msg,
						}, function(error, result){
							if (error) {
								console.log(error);
							}
						});
					});

					var $id= $('.chat');
					$id.scrollTop($id[0].scrollHeight);
				}
			});
		}
	},
	'click #camera': function( event, template ) {
		var cameraOptions = {
			width: 800,
			height: 600,
			quality: 75
		};

		MeteorCameraUI.getPicture(cameraOptions, function (error, data) {
			if (error) {
				document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: error.reason });
			} else {
				var phot = MeteorCameraUI.dataURIToBlob(data);
				var ext = phot.type.split(/\//).pop();
				phot.name = Meteor.userId() + '-' + Date.now() + '.' + ext;
				var uploader = new Slingshot.Upload("myFileUploads");
				NProgress.start();

				uploader.send(phot, function (errorr, downloadUrl) {
					if (errorr) {
						document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: uploader.xhr.response });
						document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: errorr.reason });
					} else {
						Meteor.call('newImgMessage', {
							image: downloadUrl,
							type: 'image',
							chatId: Session.get('chatId')
						}, function(errorrr, result){
							if (errorrr) {
								document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: errorrr.reason });
							} else {
								document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'Upload Complete' });
								var $id= $('.chat');
								$id.scrollTop($id[0].scrollHeight);
								NProgress.done();
							}
						});
					}
				});
			}
		});
	},
	'click .responsive-img': function( event, template ) {
		event.preventDefault();
		$('.responsive-img').magnificPopup({
			items: {
				src: event.currentTarget.currentSrc,
				type: 'image'
			}
		}).magnificPopup('open');
	}
});