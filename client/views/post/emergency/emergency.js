Template.emergencyPopup.onCreated( function() {
	this.subscribe('members');
});

Template.emergency.events({
	'click .emer': function( event ) {
		event.preventDefault();
		var dialog = document.querySelector('dialog');
		if (! dialog.showModal) {
			dialogPolyfill.registerDialog(dialog);
		}
		dialog.showModal();
	}
});

Template.emergencyPopup.events({
	'click .send': function( event, template ) {
		var useBelong = Meteor.users.find({ 'userDetails.members.userId': Meteor.userId() }).fetch();

		if( Meteor.user() && Meteor.user().userDetails && Meteor.user().userDetails.members ) {
			Meteor.call("userNotification", {
				title: 'Emergency',
				text: Meteor.user().userDetails.profile.name + ' sent you the emergency notification1.',
				userId: Meteor.user().userDetails.members[0].userId
			});
		} else if(useBelong) {
			Meteor.call("userNotification", {
				title: 'Emergency',
				text: Meteor.user().userDetails.profile.name + ' sent you the emergency notification2.',
				userId: useBelong[0]._id
			});
		} else {
			document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'Error sending notification to family member.'});
		}
		Meteor.call(
			'sendEmail',
			'app.carebuddy@gmail.com',
			Meteor.user().username,
			'notification from ' + Meteor.user().username,
			'This mail is regarding the emergency notiication from ' + Meteor.user().username+ '. Contact them for their well being.',
			function(cbError, cbResponse) {
				if(cbError) {
					document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: cbError.reason});
				} else {
					document.querySelector('dialog').close();
					document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'Notfication sent.' });
				}
			});
	},
	'click .close': function() {
		document.querySelector('dialog').close();
	}
});