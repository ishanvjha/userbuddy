Template.editProfile.onRendered(function() {
	Tracker.autorun(function(){
		if(Meteor.user() && Meteor.user().userDetails && Meteor.user().userDetails.profile) {
			$('[name=name]').val(Meteor.user().userDetails.profile.name);
			$('[name=dob]').val(Meteor.user().userDetails.profile.dob);
			$('[name=blood]').val(Meteor.user().userDetails.profile.blood);
			$('[name=allergies]').val(Meteor.user().userDetails.profile.allergies);
		}
	});
	$('#profile-form').validate({
		rules: {	
			name: {
				required: true
			},
			dob: {
				required: true,
				date: true
			}
		}, 
		messages: {
			name: {
				required: "Name is required."
			},
			dob: {
				required: "Date of birth is required.",
				date: "Valid date is required."
			}
		},
		submitHandler() {

			Meteor.call('userDetailsUpdate', {
				name:  $('[name="name"]').val(),
				dob: $('[name="dob"]').val(),
				blood:  $('[name="blood"]').val(),
				allergies: $('[name="allergies"]').val()
			}, function(error, result){
				if (error) {
					Bert.alert( error.reason, 'danger', 'growl-top-right' );
				} else {
					document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'Profile updated' });
					document.getElementById('profile-form').reset();
					document.querySelector('dialog#editProfile').close();
				}
			});
		}
	});
});

Template.editProfile.events({
	detail: function () {
		if( Meteor.user() ) {
			return Meteor.user();
		}
	}
});

Template.editProfile.events({
	'submit form': function(event) {
		event.preventDefault();
	},
	'click .close': function() {
		document.querySelector('dialog').close();
	}
});