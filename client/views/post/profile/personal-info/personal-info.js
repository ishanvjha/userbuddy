Template.personalInfo.helpers({
	detail: function () {
		if( Meteor.user() ) {
			return Meteor.user();
		}
	}
});

Template.personalInfo.events({
	'click .show-editProfile-modal': function () {
		var dialog = document.querySelector('dialog#editProfile');
		if (! dialog.show) {
			dialogPolyfill.registerDialog(dialog);
		}
		dialog.show();
	},
	'click .close-editProfile': function () {
		var dialog = document.querySelector('dialog#editProfile');
		dialog.close();
	}
});