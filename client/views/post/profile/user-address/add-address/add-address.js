Template.addAddress.onRendered(function() {
	$('#address-form').validate({
		rules: {	
			pinCode: {
				required: true
			},
			name: {
				required: true
			},
			address1: {
				required: true
			},
			city: {
				required: true
			},
			state: {
				required: true
			},
			phone: {
				required: true,
				minlength: 10
			}
		}, 
		messages: {
			pinCode: {
				required: "Pin Code is required."
			},
			name: {
				required: "Name is required."
			},
			address1: {
				required: "Address is required."
			},
			city: {
				required: "City is required."
			},
			state: {
				required: "State is required."
			},
			phone: {
				required: "Phone is required.",
				minlength: "Not a valid number"
			}
		},
		submitHandler() {
			Meteor.call('userAddress', {
				pinCode:  $('[name="pinCode"]').val(),
				name:  $('[name="name"]').val(),
				address1:  $('[name="address1"]').val(),
				address2: $('[name="address2"]').val(),
				city: $('[name="city"]').val(),
				state:  $('[name="state"]').val(),
				phone: $('[name="phone"]').val()
			}, function(error, result){
				if (error) {
					Materialize.toast( error.reason, 2000, 'rounded');
				} else {
					document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'Address added' });
					document.getElementById('address-form').reset();
					document.querySelector('dialog#addAddress').close();
				}
			});
		}
	});
});

Template.addAddress.events({
	'submit form': function(event) {
		event.preventDefault();
	},
	'click .close-addAddress': function () {
		var dialog = document.querySelector('dialog#addAddress');
		dialog.close();
	}
});