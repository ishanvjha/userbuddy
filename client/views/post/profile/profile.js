Template.profile.onRendered(function() {
	Tracker.autorun(function(){
		if(Meteor.user() && Meteor.user().userDetails && Meteor.user().userDetails.profile && Meteor.user().userDetails.profile.name) {
			var name = Meteor.user().userDetails.profile.name;
		} else {
			var name = 'CareBuddy';
		}
		$('.member-img').initial({
			name: name,
			height: 46,
			width: 46,
			charCount: 1,
			textColor: "#fff",
			fontSize: 20,
			fontWeight: 400,
			radius: 6,
			seed: 2
		});
	});
});

Template.profile.helpers({
	detail: function () {
		return Meteor.user();
	}
});