Template.recordValues.onRendered(function() {
	$('#record-values').validate({
		rules: {	
			time: {
				required: true
			},
			date: {
				required: true,
				date: true
			},
			values: {
				required: true
			},
			signs: {
				required: true
			},
			measurement: {
				required: true
			},
			valUser: {
				required: true
			}
		}, 
		messages: {
			time: {
				required: "Time is required."
			}, 
			date: {
				required: "Date is required.",
				date: "Not a valid date."
			},
			values: {
				required: "Enter the value."
			},
			signs: {
				required: "Signs is required."
			},
			measurement: {
				required: "Measurement is required."
			},
			valUser: {
				required: "User not selected."
			}, 
		},
		submitHandler() {
			const valUser = $('[name=valUser]').val(),
			time = $('[name=time').val(),
			date = $('[name=date]').val(),
			values = $('[name=values').val(),
			signs = $('[name=signs]').val(),
			measurement = $('[name=measurement]').val(),
			comments = $('[name=comments').val();

			if (_.isEmpty(valUser)) {
				Bert.alert( 'User not selected', 'danger', 'growl-top-right' );
				return;
			}

			Uploads.insert({
				type: 'value',
				user: valUser,
				time: time,
				date: date,
				values: values,
				signs: signs,
				measurement: measurement,
				comments: comments,
				createdAt : new Date()
			}, function( error, response ) {
				if(error) {
					Bert.alert( error.reason, 'danger', 'growl-top-right' );
				} else {
					Bert.alert( 'Document uploaded successfully.', 'info', 'growl-top-right' );
					document.getElementById('record-values').reset();
					document.querySelector('dialog#modal-value').close();
				}
			});
		}
	});
});

Template.recordValues.helpers({
	user: function() {
		if( Meteor.user() && Meteor.user().userDetails && Meteor.user().userDetails.profile && Meteor.user().userDetails.profile.name ) {
			const userProfile = {
				userId: Meteor.user()._id,
				userName: Meteor.user().userDetails.profile.name
			};
			return userProfile;
		} else {
			const userProfile = {
				userId: Meteor.user()._id,
				userName: Meteor.user().username
			};
			return userProfile;
		}
	},
	allMembers: function () {
		if( Meteor.user() && Meteor.user().userDetails && Meteor.user().userDetails.members ) {
			return Meteor.user().userDetails.members;
		}
	}
});

Template.recordValues.events({
	'submit form': function( event, template ) {
		event.preventDefault();
	},
	'click .close': function() {
		var dialog = document.querySelector('dialog#modal-value');
		dialog.close();
	}
});