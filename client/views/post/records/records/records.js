Template.records.onRendered(function() {
	var self = this;
	self.autorun(function() {
		self.subscribe('records');  
	});
});

Template.records.helpers({
	records: function() {
		return Uploads.find({ user: Meteor.userId() }).fetch();
	}
});

Template.records.events({
	'click .drop-head': function(event, template) {
		if ( $(event.currentTarget).hasClass('active')) {
			$(event.currentTarget.nextElementSibling).stop().slideUp(200);
			$(event.currentTarget).removeClass('active');
		} else {
			$('.drop-head').removeClass('active');
			$('.drop-body').slideUp(200);
			$(event.currentTarget.nextElementSibling).stop().slideToggle(200);
			$(event.currentTarget).addClass('active');
		}
	},
	'click .responsive-img': function(event, template) {
		$('.responsive-img').magnificPopup({
			items: {
				src: event.currentTarget.currentSrc,
				type: 'image'
			}
		}).magnificPopup('open');
	}
});