Template.recordDetails.onRendered(function() {
	$('#records-detail').validate({
		rules: {	
			imgUser: {
				required: true
			},
			imgDate: {
				required: true,
				date: true
			}
		}, 
		messages: {
			imgUser: {
				required: "Select a user."
			}, 
			imgDate: {
				required: "Date is required.",
				date: "Not a valid date."
			}
		},
		submitHandler() {
			const comments = $('[name=imgComments').val(),
			record = $('[name=imgRecord]').val(),
			date = $('[name=imgDate').val(),
			user = $('[name=imgUser]').val();
			
			if(_.isEmpty(Session.get('url'))) {
				Bert.alert( 'Document upload in progress', 'danger', 'growl-top-right' );
			} else {
				Uploads.insert({
					url: Session.get('url'),
					type: 'image',
					user: user,
					date: date,
					record: record,
					comments: comments,
					createdAt : new Date()
				}, function(eor,ros) {
					if(eor) {
						Bert.alert( eor.reason, 'danger', 'growl-top-right' );
					} else {
						Bert.alert( 'Document uploaded successfully', 'info', 'growl-top-right' );
					}
				});
				document.querySelector('dialog#modal-detail').close();
				document.getElementById('records-detail').reset();
				Session.clear('url');
			}
		}
	});
});

Template.recordDetails.helpers({
	// img: function() {
	// 	return Session.get('uploadPreview');
	// },
	user: function() {
		if( Meteor.user() && Meteor.user().userDetails && Meteor.user().userDetails.profile && Meteor.user().userDetails.profile.name ) {
			const userProfile = {
				userId: Meteor.user()._id,
				userName: Meteor.user().userDetails.profile.name
			};
			return userProfile;
		} else {
			const userProfile = {
				userId: Meteor.user()._id,
				userName: Meteor.user().username
			};
			return userProfile;
		}
	},
	allMembers: function () {
		if( Meteor.user() && Meteor.user().userDetails && Meteor.user().userDetails.members ) {
			return Meteor.user().userDetails.members;
		}
	}
});

Template.recordDetails.events({
	'submit form': function( event ) {
		event.preventDefault();
	},
	'click .close': function() {
		document.querySelector('dialog#modal-detail').close();
	}
});