Template.recordUpload.events({
	'click .camera': function( event, template ) {
		var dialog = document.querySelector('dialog#modal-image');
		if (! dialog.close) {
			dialogPolyfill.registerDialog(dialog);
		}

		var dialogDetails = document.querySelector('dialog#modal-detail');
		if (! dialogDetails.show) {
			dialogPolyfill.registerDialog(dialogDetails);
		}

		var cameraOptions = {
			width: 800,
			height: 800,
			quality: 100
		};
		MeteorCameraUI.getPicture(cameraOptions, function (camError, data) {

			if (camError) {
				Bert.alert( camError.reason, 'danger', 'growl-top-right' );
			} else {

				file = MeteorCameraUI.dataURIToBlob(data);
				file.name =  Meteor.userId() + '-' + Date.now() + '.' + file.type.split(/\//).pop();

				if(file) {
					NProgress.start();

					Session.set('url', '');
					var uploader = new Slingshot.Upload("myFileUploads");

					uploader.send(file, function (upError, response) {

						if (upError) {
							NProgress.remove();
							Bert.alert( uploader.xhr.response, 'danger', 'growl-top-right' );
							Bert.alert( upError.reason, 'danger', 'growl-top-right' );
						} else {

							Tracker.autorun(function (computation) {
								if (! isNaN(uploader.progress())){
									console.log(uploader.progress())
									NProgress.set(uploader.progress());
									return Session.set('uploadResponse', Math.round(uploader.progress() * 100));
								}
							});
							Bert.alert( 'Upload complete', 'info', 'growl-top-right' );
							Session.set('url', response);
							dialog.close();
							dialogDetails.show();
							Session.set('uploadPreview', uploader.url(true));
						}
					});
				}
			}
		});
	},
	'change #myFileInput': function( event, template ) {
		var dialog = document.querySelector('dialog#modal-image');
		if (! dialog.close) {
			dialogPolyfill.registerDialog(dialog);
		}

		var dialogDetails = document.querySelector('dialog#modal-detail');
		if (! dialogDetails.show) {
			dialogPolyfill.registerDialog(dialogDetails);
		}

		var file = document.getElementById('myFileInput').files[0];
		NProgress.start();
		
		result = Blaze._globalHelpers.camera(file);

		Tracker.autorun(function (computation) {
			if (! isNaN(result.progress())){
				NProgress.set(result.progress());
				return Session.set('uploadResponse', Math.round(result.progress() * 100));
			}
		});

		dialog.close();
		dialogDetails.show();
	},
	'click .close': function() {
		document.querySelector('dialog#modal-image').close();
	}
});