Template.newReminder.onRendered(function() {
	this.subscribe('members');

	$('#reminder-form').validate({
		rules: {	
			user: {
				required: true
			},
			title: {
				required: true
			},
			drug: {
				required: true
			},
			reminder: {
				required: true
			},
			frequency: {
				required: true
			},
			dose: {
				required: true
			},
			time: {
				required: true
			},
			createdAt: {
				required: true,
				date: true
			}
		}, 
		messages: {
			user: {
				required: "User is required."
			},
			title: {
				required: "Title is required."
			},
			drug: {
				required: "Drug is required."
			},
			reminder: {
				required: "Reminder details required."
			},
			frequency: {
				required: "Frequency of doasge is required."
			},
			dose: {
				required: "Dose is required."
			},
			time: {
				required: "Time is required."
			},
			medUser: {
				required: "Select a patient.",
				date: "Date is required."
			}
		},
		submitHandler() {
			const user = $('[name=user]').val(),
			title = $('[name=title]').val(),
			drug = $('[name=drug]').val(),
			frequency = $('[name=frequency]').val(),
			dose = $('[name=dose]').val(),
			time = $('[name=time]').val(),
			reminder = $('[name=reminder]').val();

			Reminders.insert({
				user: user,
				title: title,
				drug: drug,
				frequency: frequency,
				dose: dose,
				reminder: reminder,
				createdAt: new Date()
			}, function(error, result){
				if (error) {
					toastr.error(error.reason);
				} else {
					toastr.success('Reminder created.');
					$('.record-full').fadeOut();
					document.getElementById('reminder-form').reset();
				}
			});
		}
	});
});

Template.newReminder.events({
	'click #close': function( event, template ) {
		$('.record-full').fadeOut();
	},
	'submit form': function( event, template ) {
		event.preventDefault();
	}
});