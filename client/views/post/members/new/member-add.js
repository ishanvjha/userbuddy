Template.newMember.onRendered(function() {
	$('#member-form').validate({
		rules: {	
			name: {
				required: true
			},
			phone: {
				required: true,
				minlength: 10
			},
			date:  {
				required: true,
				date: true
			},
			relation:  {
				required: true
			},
			gender: {
				required: true
			},
			phone: {
				required: true,
				minlength: 10
			}
		}, 
		messages: {
			name: {
				required: "Member's name is required."
			}, 
			phone: {
				required: "Phone Number is required.",
				minlength: jQuery.validator.format("Please, at least {0} characters are necessary")
			},
			date:  {
				required: "Date of birth is required.",
				date: "Not a valid date of birth."
			},
			relation:  {
				required: "Choose a relation."
			},
			gender: {
				required: "Choose member's gender."
			},
			phone: {
				required: "Phone is required.",
				minlength: "Not a valid number"
			}
		},
		submitHandler() {
			const options = {
				username: $('[name=phone]').val(),
				password: $('[name=phone]').val(),
			};

			const name = $('[name=name]').val(),
			phone = $('[name=phone]').val(),
			date = $('[name=date]').val(),
			relation = $('[name=relation]').val(),
			gender = $('[name=gender]').val();

			const member = {
				name: name,
				phone: phone,
				date: date,
				relation: relation,
				gender: gender
			};

			const newRelation = {
				type: $('[name=relation]').val(),
				user: Meteor.userId()
			};

			Meteor.call('newMember', options, function(error, response){
				if (error) {
					Bert.alert( error.reason, 'danger', 'growl-top-right' );
				} else {
					Meteor.call('relation', {
						type: $('[name=relation]').val(),
						userId: response,
						name: name,
						phone: phone
					}, function(relError, relResult){
						if (relError) {
							document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: relError.reason });
						} else {
							Meteor.call('memberDetailsUpdate', {
								name: $('[name=name]').val(),
								dob: $('[name=dob]').val(),
								gender: $('[name=gender]').val(),
								userId: response
							}, function(erorr, result){
								if (erorr) {
									document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: erorr.reason });
								} else {
									Meteor.call('newmemberText', $('[name=phone]').val(), function(errorr, responsee){
										if (errorr) {
											document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: errorr.reason });
										} else {
											document.querySelector('.mdl-js-snackbar').MaterialSnackbar.showSnackbar({ message: 'Member added.' });
											document.querySelector('dialog').close();
											document.getElementById('member-form').reset();
											Meteor.call( 'cbMail', 'New Member', member);
										}
									});
								}
							});
						}
					});
				}
			});
		}
	});
});

Template.newMember.events({
	'submit form': function( event ) {
		event.preventDefault();
	},
	'click .close': function( event ) {
		var dialog = document.querySelector('dialog#memberDialog');
		dialog.close();
	}
});