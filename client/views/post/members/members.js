Template.members.helpers({
	allMembers: function () {
		if( Meteor.user() && Meteor.user().userDetails && Meteor.user().userDetails.members ) {
			return Meteor.user().userDetails.members;
		}
	}
});

Template.members.events({
	'click .show-members-modal': function( event ) {
		event.preventDefault();
		var dialog = document.querySelector('dialog#memberDialog');
		if (! dialog.show) {
			dialogPolyfill.registerDialog(dialog);
		}
		dialog.show();
	}
});