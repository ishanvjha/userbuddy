NProgress.configure({ 
	easing: 'ease', 
	speed: 500,
	minimum: 0.1,
	trickleRate: 0.02,
	trickleSpeed: 600,
	showSpinner: false
});

Bert.defaults = {
	hideDelay: 2000,
	style: 'fixed-bottom',
	type: 'default'
};