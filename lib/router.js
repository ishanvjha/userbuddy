//dataReadyHold = null;

if (Meteor.isClient) {
	//dataReadyHold = LaunchScreen.hold();

	BlazeLayout.setRoot('body');

	AccountsTemplates.configureRoute('signIn', {
		layoutType: 'blaze',
		name: 'signin',
		path: '/login',
		template: 'login',
		layoutTemplate: 'appLayout',
		layoutRegions: {
			top: 'siteTitle',
			template: "login"
		},
		contentRegion: 'main'
	});

	AccountsTemplates.knownRoutes.push('logout');
		
	Accounts.onLogin(function() {
		var path = FlowRouter.current().path;

		if(path === "/" || "/login" || "/register"){
			FlowRouter.go("/dashboard/home");
		}
	});

	FlowRouter.subscriptions = function() {
		this.register('user', Meteor.subscribe('userData'));
	};
}

FlowRouter.route('/', {
	name: 'Welcome',
	action: function() {
		BlazeLayout.render( "appLayout", { top: "siteTitle", template: "welcome" });
	},
	fastRender: true
});

FlowRouter.route('/login', {
	name: 'login',
	action: function() {
		BlazeLayout.render( "appLayout", { dialog: "forgot", template: "login" });
	},
	fastRender: true
});

FlowRouter.route('/register', {
	name: 'register',
	action: function() {
		BlazeLayout.render( "appLayout", { template: "register" });
	},
	fastRender: true
});

FlowRouter.route('/verifyPhone', {
	name: 'phoneNumberVerification',
	action: function(queryParams) {
		BlazeLayout.render( "appLayout", { template: "verifyPhone" });
	},
	fastRender: true
});

FlowRouter.route('/password', {
	name: 'password',
	action: function(queryParams) {
		BlazeLayout.render( "appLayout", { template: "password" });
	},
	fastRender: true
});

var dashboard = FlowRouter.group({
	prefix: '/dashboard',
	name: 'dashboard'
});

dashboard.route('/home', {
	name: 'home',
	triggersEnter: [AccountsTemplates.ensureSignedIn],
	action: function() {
		BlazeLayout.render( "dashLayout", { template: "home" });
	},
	subscriptions: function(params, queryParams) {
		this.register('chats', Meteor.subscribe('chats', Meteor.userId()));
	}
});

dashboard.route('/records', {
	name: 'records',
	triggersEnter: [AccountsTemplates.ensureSignedIn],
	action: function() {
		BlazeLayout.render( "dashLayout", { template: "record" });
	}
});

dashboard.route('/reminders', {
	name: 'reminders',
	triggersEnter: [AccountsTemplates.ensureSignedIn],
	action: function() {
		BlazeLayout.render( "dashLayout", { template: "reminders" });
	}
});

dashboard.route('/members', {
	name: 'members',
	triggersEnter: [AccountsTemplates.ensureSignedIn],
	action: function() {
		BlazeLayout.render( "dashLayout", { dialog: "newMember", template: "members" });
	}
});

dashboard.route('/medicine', {
	name: 'medicine',
	triggersEnter: [AccountsTemplates.ensureSignedIn],
	action: function() {
		BlazeLayout.render( "dashLayout", { dialog: "medicineOrder", template: "medicine" });
	}
});

dashboard.route('/appointments', {
	name: 'appointments',
	triggersEnter: [AccountsTemplates.ensureSignedIn],
	action: function() {
		BlazeLayout.render( "dashLayout", { template: "appointments" });
	}
});

dashboard.route('/profile', {
	name: 'profile',
	triggersEnter: [AccountsTemplates.ensureSignedIn],
	action: function() {
		BlazeLayout.render( "dashLayout", { template: "profile" });
	},
	fastRender: true
});

dashboard.route('/emergency', {
	name: 'emergency',
	triggersEnter: [AccountsTemplates.ensureSignedIn],
	action: function() {
		BlazeLayout.render( "dashLayout", { dialog: "emergencyPopup", template: "emergency" });
	},
	fastRender: true
});

FlowRouter.route('/verify-email/:verifyToken', {
	name: 'atVerifyEmail',
	action: function(params) {
		if(! _.isEmpty( params.verifyToken) ) {
			Accounts.verifyEmail( params.verifyToken, ( error ) =>{
				if ( error ) {
					console.log(error);
				}
			});
		} else {
			FlowRouter.go("/login");
		}
	},
	fastRender: true
});

FlowRouter.route('/reset-password/:resetToken', {
	name: 'atResetPwd',
	action: function(params) {
		BlazeLayout.render( "appLayout", { template: "resetPassword" });
	},
	fastRender: true
});

FlowRouter.route('/forgot-password', {
	name: 'atForgotPwd',
	action: function(params) {
		BlazeLayout.render( "appLayout", { template: "forgotPassword" });
	},
	fastRender: true
});

FlowRouter.route('/logout', {
	action: AccountsTemplates.logout
});

FlowRouter.notFound = {
	action: function() {
		if(Meteor.user()) {
			FlowRouter.go("/dashboard/home");
		} else {
			FlowRouter.go("/login");
		}
	}
};